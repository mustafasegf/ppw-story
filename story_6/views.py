from django.http import request
from django.shortcuts import redirect, render
from .forms import PeopleForm
from .models import People, Activities


def index(request):
    people = People.objects.all()
    activities = Activities.objects.all()
    context = {'people': people, 'activities': activities, 'page_title' : 'Activities'}
    return render(request, 'story_6/index.html', context)


def register(request):
    form = PeopleForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('story_6:index')
    context = {'page_title': 'register', 'form':form}
    return render(request, "story_6/register.html", context)
