from django.db.models import fields
from django.forms import ModelForm
from .models import People


class PeopleForm(ModelForm):
    class Meta:
        model = People
        fields = ['people_name', 'activities']
