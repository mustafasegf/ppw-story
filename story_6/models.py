from django.db import models


class Activities(models.Model):
    activities_name = models.CharField(max_length=100)
    def __str__(self):
        return self.activities_name


class People(models.Model):
    people_name = models.CharField(max_length=100)
    activities = models.ForeignKey(Activities, on_delete=models.CASCADE)
    def __str__(self):
        return self.people_name
