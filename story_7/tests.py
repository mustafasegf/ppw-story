from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.


class TestCaseStory7(TestCase):
    def test_root_url_check(self):
        response = Client().get(reverse('story_7:index'), follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_views_check(self):
        response = Client().get(reverse('story_7:index'), follow=True)
        self.assertIn("Accordion", str(response.content))
    
