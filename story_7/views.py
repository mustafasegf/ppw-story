from django.http import request
from django.shortcuts import render


def index(request):
    context = {'page_title' : 'Activities'}
    return render(request, 'story_7/index.html', context)