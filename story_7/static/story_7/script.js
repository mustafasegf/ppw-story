$('.parent').click(function() {
  var $this = $(this).parent().next();

  $this.toggleClass('show');
  $this.slideToggle(350);
});

$('.down').click(function() {
  let $this = $(this).parent().parent();
  $this.next().insertBefore($this);
});

$('.up').click(function() {
  let $this = $(this).parent().parent();
  $this.prev().insertAfter($this);
});