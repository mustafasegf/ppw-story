from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.


class TestCaseStory8(TestCase):
    def test_root_url_check(self):
        response = Client().get(reverse('story_9:index'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_index_views_check(self):
        response = Client().get(reverse('story_9:index'), follow=True)
        self.assertIn("Register", response.content.decode('utf-8'))

    def test_create_account(self):
        response = Client().post(reverse('story_9:index'), {
            'username': 'test123',
            'email': 'test@gmail.com',
            'first_name': 'testing',
            'password1': 'NtW6XJ6YtV8iKNc',
            'password2': 'NtW6XJ6YtV8iKNc'
        }, follow=True)
        self.assertIn("Welcome back", response.content.decode('utf-8'))

    def test_create_failed_account(self):
        response = Client().post(reverse('story_9:index'), {
            'username': 'test',
            'email': 'test@',
            'first_name': 'test',
            'password1': 'a',
            'password2': 'a'
        }, follow=True)
        self.assertIn("failed to create account", response.content.decode('utf-8'))
