from django.http import response
from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login


def index(request):
    form = CreateUserForm()
    message = "Create your account"
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(username=form.cleaned_data.get("username"),
            password=form.cleaned_data.get("password1"))
            login(request, user)
        else:
            message = "⚠️failed to create account"
            form = CreateUserForm()
    context = {'page_title': 'Login', 'form' : form, 'message':message}
    return render(request, 'story_9/index.html', context)