from django.urls import path
from . import views

app_name = 'story_5'
urlpatterns = [
    path('', views.index, name='index'),
    path('create_matkul', views.create_course, name="create_course"),
    path('<int:id>', views.course_detail, name="course_detail"),
    path('view-matkul/<int:id>/delete', views.delete_course, name="delete_course"),
    
]
