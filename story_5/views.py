from django.http import request
from django.shortcuts import redirect, render
from .forms import MataKuliahForm
from .models import MataKuliah

# Create your views here.


def index(request):
    mata_kuliah = MataKuliah.objects.all()
    context = {'page_title': 'Story 5', 'mata_kuliah': mata_kuliah}
    return render(request, "story_5/index.html", context)


def create_course(request):
    form = MataKuliahForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('story_5:index')
    context = {'page_title': "Create Course", 'form': form}
    return render(request, 'story_5/create_course.html', context)


def course_detail(request, id):
    mata_kuliah = MataKuliah.objects.get(id=id)
    context = {'page_title': mata_kuliah.nama, 'mata_kuliah': mata_kuliah}
    return render(request, 'story_5/course_detail.html', context)


def delete_course(request, id):
    mata_kuliah = MataKuliah.objects.get(id=id)
    mata_kuliah.delete()
    return redirect('story_5:index')
