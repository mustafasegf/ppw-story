from story_5.models import MataKuliah, Semester
from django.contrib import admin

# Register your models here.
admin.site.register(Semester)
admin.site.register(MataKuliah)
